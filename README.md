# devops-chalenge-tw



example used :  https://github.com/contentful/the-example-app.nodejs

kaniko executor used to execute dockerfile to build the image of application and push to registry .
(Documentation: https://docs.gitlab.com/ee/ci/docker/using_kaniko.html )

gcloud used to demonstrate deployment ( I used my own service account and cluster , if necessary , ask for access in my gitlab repo to trigger the example test)

Dockerfile in root used to build the image used by gitlab CI with dependendencies ( nodejs,npm,docker,kubernetes and gcloud SDK . It's necessary to build it, push to a registry and reference it in  .gitlab-ci.yml  or you can use from here faelnpaiva/ubuntu_nodejs_npm_docker_gcp_kubeneters hosted by dockerhub).

Any other branch except master will be deployed in staging cluster . 
Master commits trigger the jobs , but I let the deploying as manual to deploy in production cluster.

If you create a new branch , before trigger it , go to REPOSITORY menu and allow the branch as PROTECTED to Owner + Developers . Without this, the branch won't get the variables setup.

TAG of docker images are referenced by pipeline number